#!/bin/bash

TEMPLATES=(
    '/opt/jboss/keycloak/themes/base/login/template.ftl'
    '/opt/jboss/keycloak/themes/base/admin/index.ftl'
    '/opt/jboss/keycloak/themes/base/account/template.ftl'
)

set -e

for TEMPLATE_FILE in "${TEMPLATES[@]}"; do
    echo "Add favicon definicion in ${TEMPLATE_FILE}"
    # Cremos un arhivo temporal
    TEMP_FILE="$(mktemp)"

    # Remplazamos el template
    sed $'/^[[:space:]]*<link[[:space:]]\+.\+favicon\.ico.*>[[:space:]]*$/{r /opt/jboss/keycloak/themes/contento/favicon-contento-fix.ftl\nd}' \
        "${TEMPLATE_FILE}" \
        > "${TEMP_FILE}"

    mv "${TEMP_FILE}" "${TEMPLATE_FILE}"

    grep -F '<#-- BEGIN - FAVICON -->' "${TEMPLATE_FILE}" > /dev/null
done
