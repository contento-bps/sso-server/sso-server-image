docker build -t sso-server-image . \
&& \
docker run --rm --name keycloak-theme -it \
-e KEYCLOAK_DEFAULT_THEME=contento \
-e KEYCLOAK_WELCOME_THEME=contento \
-e DB_VENDOR=h2 \
-e KEYCLOAK_USER=admin \
-e KEYCLOAK_PASSWORD=admin \
-e PROXY_ADDRESS_FORWARDING \
-p 9080:8080 \
-p 9443:8443 \
-v keycloak-build-run:/opt/jboss/keycloak/standalone/data \
sso-server-image         

