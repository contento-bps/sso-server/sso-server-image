﻿<#
.Synopsis
Crea la estructura de las unidades organizativas en Active Drectory, que corresponde a Roles, Grupos, aplicacione sy roles de aplicaciones en el SSO Server.
La estructura es especificada en un archivo JSON exportado desde el projexto bigdata.


.Description
Este script es para crear en Active Directory la estructura de OU usando PowerShell. El script tomará la entrada de un archivo JSON y Creara
la estrutura de roles, grups applicaciones y roles de aplicaciones especificadas en el json.

Por cada Role en el Realm del SSO corresponde un Grupo en AD.
Por cada Grupo en el Realm del SSO corresponde un Grupo en el AD.
Los roles de las aplicaciones tambien son representado como grupos.

Ademas existen Roles de aplicaciones que se especificaran

.Example
Sync-Ou-Ldap -JsonlFile G:\sync-ou-ldap.json -OuRealms 'ou=realms,ou=sso,dc=test,dc=contento,dc=com,dc=co' -ProtectedFromAccidentalDeletion 0

Este comando creará una estructura OU a partir de una entrada JsonlFile. 
El parametro OuRealms especifica una unidad organizativa que sera la raiz o la unidad organizativa padre, desde donde empieza a crearse la estructura en el json.

Author: Yohany Flores (yohanyflores@gmail.com)
#>

function Sync-Ou-Ldap {
    [Cmdletbinding()]
    Param(
        [Parameter(Mandatory = $True, HelpMessage = 'Proporcione la ruta del archivo Json que contiene la estructura OU')]
        [String]$JsonFile,
        [Parameter(Mandatory = $True, HelpMessage = 'Proporcione la ruta principal de OU')]
        [String]$OuRealms,
        [Parameter(Mandatory = $False, HelpMessage = 'Prefijo para el SamAccountName.')]
        [String]$SamAccountPrefx = "contento",
        [Parameter(Mandatory = $False, HelpMessage = 'Protege las Unidades Organizativas creadas, para evitar que se elimine la unidad organizativa por equivocacion.')]
        [Boolean]$ProtectedFromAccidentalDeletion = $False
    )

    # [System.IO.Directory]::SetCurrentDirectory($PSScriptRoot)
    $jsonData = (Get-Content "$JsonFile" | Out-String | ConvertFrom-Json)

    foreach ($realm in $jsonData.PSObject.Properties) {

        $OuRealm = "ou=$($realm.Name),${OuRealms}"
        $SamAccountNameRealm = "$SamAccountPrefx-sso-realms-$($realm.Name)"


        if ([adsi]::Exists("LDAP://$OuRealm") -eq "True") {
            Write-host -ForegroundColor Yellow "'$($realm.Name)' ya existe en la ubicación ${OuRealms}"
            Set-ADOrganizationalUnit -Identity $OuRealm -Description "Estructura para el SSO Server." -ProtectedFromAccidentalDeletion $ProtectedFromAccidentalDeletion -ErrorAction Stop
        } else {
            Write-Output "Creating the OU $OuRealm ....."
            New-ADOrganizationalUnit -Name $realm.Name -Path $OuRealms -Description "Estructura para el SSO Server." -ProtectedFromAccidentalDeletion $ProtectedFromAccidentalDeletion -ErrorAction Stop
            Write-Host -ForegroundColor Green "OU $($realm.Name) fué creado en la ubicación ${OuRealms}"
        }

        $realmObj = $realm.Value

        # Grupos del Realm
        if ([bool]($realmObj.PSobject.Properties.name -match "groups")) {
            $OUGrupos = "ou=Grupos,$OuRealm"

            if ([adsi]::Exists("LDAP://$OUGrupos") -eq "True") {
                Set-ADOrganizationalUnit -Identity $OUGrupos -Description "Grupos del SSO Server" -ProtectedFromAccidentalDeletion $ProtectedFromAccidentalDeletion -ErrorAction Stop
            } else {
                New-ADOrganizationalUnit -Name "Grupos" -Path $OuRealm -Description "Grupos del SSO Server" -ProtectedFromAccidentalDeletion $ProtectedFromAccidentalDeletion -ErrorAction Stop
            }

            if ($realmObj.groups -is [array]) {
                $groups = $realmObj.groups
            } else {
                $groups = @( [string]$realmObj.groups )
            }

            foreach ($group in $groups) {
                if ($group -is [string]) {
                    $GroupName = $group
                    $GroupDesc = $group
                } else {
                    $GroupName = $group.name
                    $GroupDesc = $group.desc
                }

                $PathGroup = "CN=$GroupName,$OUGrupos"
                $SamAccountNameGroup = "$SamAccountNameRealm-groups-$GroupName"
                
                Write-Output "Crear Grupo: $PathGroup"

                if ([adsi]::Exists("LDAP://$PathGroup") -eq "True") {
                    Write-host -ForegroundColor Yellow "Group $GroupName ya existe en la ubicación $OUGrupos"
                    Set-ADGroup -Identity $PathGroup -Description "$GroupDesc" -ErrorAction Stop
                } else {
                    Write-Output "Creating the Group $group ....."

                    try {
                        New-ADGroup -Name "$GroupName" -SamAccountName "$SamAccountNameGroup" -Path "$OUGrupos" -GroupCategory Security -GroupScope Global -DisplayName "$GroupName" -Description "$GroupDesc" -ErrorAction Stop
                        Write-Host -ForegroundColor Green "Group $GroupName fué creado en la ubicación ${OUGrupos}"
                    } catch [Microsoft.ActiveDirectory.Management.ADException] {
                        Write-host -ForegroundColor Red "Group $GroupName ya existe en la ubicación ${OUGrupos}"
                    }
                }
            }
        }

        # Roles del Realm
        if ([bool]($realmObj.PSobject.Properties.name -match "roles")) {
            $OURoles = "ou=Roles,$OuRealm"

            if ([adsi]::Exists("LDAP://$OURoles") -eq "True") {
                Set-ADOrganizationalUnit -Identity $OURoles -Description "Realm Roles para el SSO Server." -ProtectedFromAccidentalDeletion $ProtectedFromAccidentalDeletion -ErrorAction Stop
            } else {
                New-ADOrganizationalUnit -Name "Roles" -Path $OuRealm -Description "Realm Roles para el SSO Server." -ProtectedFromAccidentalDeletion $ProtectedFromAccidentalDeletion -ErrorAction Stop
            }

            if ($realmObj.roles -is [array]) {
                $roles = $realmObj.roles
            } else {
                $roles = @( [string]$realmObj.roles )
            }

            foreach ($rol in $roles) {
                if ($rol -is [string]) {
                    $RolName = $rol
                    $RolDesc = $rol
                } else {
                    $RolName = $rol.name
                    $RolDesc = $rol.desc
                }

                $PathRol = "CN=$RolName,$OURoles"
                $SamAccountNameRole = "$SamAccountNameRealm-roles-$RolName"
                
                Write-Output "Crear Group: $PathRol"

                if ([adsi]::Exists("LDAP://$PathRol") -eq "True") {
                    Write-host -ForegroundColor Yellow "Group $RolName ya existe en la ubicación $OURoles"
                    Set-ADGroup -Identity $PathRol -Description "$RolDesc" -ErrorAction Stop
                } else {
                    Write-Output "Creating the Group $group ....."

                    try {
                        New-ADGroup -Name "$RolName" -SamAccountName "$SamAccountNameRole" -Path "$OURoles" -GroupCategory Security -GroupScope Global -DisplayName "$RolName" -Description "$RolDesc" -ErrorAction Stop
                        Write-Host -ForegroundColor Green "Group $RolName fué creado en la ubicación ${OURoles}"
                    } catch [Microsoft.ActiveDirectory.Management.ADException] {
                        Write-host -ForegroundColor Red "Group $RolName ya existe en la ubicación ${OURoles}"
                    }
                }
            }
        }


        # Apps del Realm
        if ([bool]($realmObj.PSobject.Properties.name -match "apps")) {
            $OUApps = "ou=Aplicaciones,$OuRealm"
            $SamAccountNameApps = "$SamAccountNameRealm-apps"

            if ([adsi]::Exists("LDAP://$OUApps") -eq "True") {
                Set-ADOrganizationalUnit -Identity $OUApps -Description "Aplicaciones en el SSO Server." -ProtectedFromAccidentalDeletion $ProtectedFromAccidentalDeletion -ErrorAction Stop
            } else {
                New-ADOrganizationalUnit -Name "Aplicaciones" -Path $OuRealm -Description "Aplicaciones en el SSO Server." -ProtectedFromAccidentalDeletion $ProtectedFromAccidentalDeletion -ErrorAction Stop
            }

            foreach ($app in $realmObj.apps.PSObject.Properties) {
                $AppName = $app.Name
                $AppValue = $app.Value

                $OuApp = "ou=$AppName,${OUApps}"
                $SamAccountNameApp = "$SamAccountNameApps-$AppName"

                $AppDesc = $AppName
                if ([bool]($AppValue.PSobject.Properties.name -match "desc")) {
                    $AppDesc = $AppValue.desc
                }
            
                if ([adsi]::Exists("LDAP://$OuApp") -eq "True") { # 
                    Write-host -ForegroundColor Yellow "'$AppName' ya existe en la ubicación ${OUApps}"
                    Set-ADOrganizationalUnit -Identity $OUApp -Description $AppDesc -ProtectedFromAccidentalDeletion $ProtectedFromAccidentalDeletion -ErrorAction Stop
                } else {
                    Write-Output "Creating the OU $OuApp ....."
                    New-ADOrganizationalUnit -Name $AppName -Path $OUApps -ProtectedFromAccidentalDeletion $ProtectedFromAccidentalDeletion -ErrorAction Stop
                    Write-Host -ForegroundColor Green "OU $AppName fué creado en la ubicación ${OUApps}"
                }

                # Roles de la aplicacion
                if ([bool]($AppValue.PSobject.Properties.name -match "roles")) {
                    $OUAppRoles = "ou=Roles,$OuApp"
                    $SamAccountNameRoles = "$SamAccountNameApp-roles"

                    if ([adsi]::Exists("LDAP://$OUAppRoles") -eq "True") {
                        Set-ADOrganizationalUnit -Identity $OUAppRoles -ProtectedFromAccidentalDeletion $ProtectedFromAccidentalDeletion -ErrorAction Stop
                    } else {
                        New-ADOrganizationalUnit -Name "Roles" -Path $OuApp -ProtectedFromAccidentalDeletion $ProtectedFromAccidentalDeletion -ErrorAction Stop
                    }
        
                    if ($AppValue.roles -is [array]) {
                        $appRoles = $AppValue.roles
                    } else {
                        $appRoles = @( [string]$AppValue.roles )
                    }
        
                    foreach ($appRol in $appRoles) {
                        if ($appRol -is [string]) {
                            $AppRolName = $appRol
                            $AppRolDesc = $appRol
                        } else {
                            $AppRolName = $appRol.name
                            $AppRolDesc = $appRol.desc
                        }
        
                        $PathAppRol = "CN=$AppRolName,$OUAppRoles"
                        $SamAccountNameRole = "$SamAccountNameRoles-$AppRolName"
                        
                        Write-Output "Crear Group: $PathAppRol"
        
                        if ([adsi]::Exists("LDAP://$PathAppRol") -eq "True") {
                            Write-host -ForegroundColor Yellow "Group $AppRolName ya existe en la ubicación $OUAppRoles"
                            Set-ADGroup -Identity $PathAppRol -Description "$AppRolDesc" -ErrorAction Stop
                        } else {
                            Write-Output "Creating the Group $group ....."
        
                            try {
                                New-ADGroup -Name "$AppRolName" -SamAccountName "$SamAccountNameRole" -Path "$OUAppRoles" -GroupCategory Security -GroupScope Global -DisplayName "$AppRolName" -Description "$AppRolDesc" -ErrorAction Stop
                                Write-Host -ForegroundColor Green "Group $AppRolName fué creado en la ubicación ${OUAppRoles}"
                            } catch [Microsoft.ActiveDirectory.Management.ADException] {
                                Write-host -ForegroundColor Red "Group $AppRolName ya existe en la ubicación ${OUAppRoles}"
                            }
                        }
                    }
                }
            }
        }
    }
}

