﻿<#
.Synopsis
Este script sirve para validar y crear una estructura de unidad organizativa basada en la entrada de un archivo JSON Lines.

Un archivo JSON Lines,  No es mas que un archivo de texto donde cada linea es un objeto json valido.


.Description
Este script es para crear en Active Directory la estructura de OU usando PowerShell. El script tomará la entrada de un archivo JSON Lines y primero validará si la OU ya existe
en la ruta mencionada en el archivo JSON Lines. Si la OU ya existe, el script lanzará un mensaje de que la OU ya existe; de lo contrario, creará la
OU en la ruta mencionada.

El script manejea dos tipos de Objetos:
La OU o unidad Organizativa especificada por el 'type': 'ou'. Que crea una unidad organizativa.
EL Grupo especificado por 'type': 'group', Que crea un Grupo.

La Sintaxis de un archivo JSON Lines se puede encontrar en http://jsonlines.org/
Un archivo JSON Lines,  No es mas que un archivo de texto donde cada linea es un objeto json valido.

.Example
Sync-OuStructure -JsonlFile E:\Scripts\OUStructureUp.jsonl -OuParent 'dc=test,dc=contento,dc=com,dc=co' -Realm 'contento-develop' -DryRun $True

Este comando creará una estructura OU a partir de una entrada JSON Lines. Cada Json en el archivo JSON Lines debe tener una propiedad llamada 'name' y 'oupath'.
oupath es el DN de la ubicacion de la OU.

Adicionalmente para el caso de grupos, debe tener una propiedad llamada 'samaccount' que es un prefijo unico para el nombre del grupo.

.Example
Un ejemplo de un archivo json line puede ser:
```
```

Author: Yohany Flores (yohanyflores@gmail.com)
#>

Function Sync-OuStructure {
    [Cmdletbinding()]
    Param(
        [Parameter(Mandatory = $True, HelpMessage = 'Please provide the path of the Json Line file containing the OU name and OU path')]
        [String]$JsonlFile,
        [Parameter(Mandatory = $True, HelpMessage = 'Please provide the OU parent path')]
        [String]$OuParent,
        [Parameter(Mandatory = $True, HelpMessage = 'Please provide the Realm Name')]
        [String]$Realm,
        [Parameter(Mandatory = $False, HelpMessage = 'Solamente chequear el archivo.')]
        [Boolean]$DryRun
    )

    #$output = Get-Content $JsonlFile | Out-String

    $regex = '^\s*$|^\s*#|^\s*\/\/'

    $listou = New-Object 'Collections.Generic.List[Object]'

    # Obtenemos el parent path e incluimos los elementos raiz.
    $parentpath = $OuParent
    foreach ($ou in @("ContentoSSO", "Realms", $Realm)) {
        $obj = [pscustomobject]@{
            type = 'ou'
            name = $ou
            oupath = $parentpath
            description = ""
        }
        $listou.Add($obj)
        $parentpath = 'ou=' + $ou + ',' + $parentpath
    }

    
    # Parseamos cada linea garantizando que todo este ok
    [System.IO.Directory]::SetCurrentDirectory($PSScriptRoot)
    $jsonstream = New-Object System.IO.StreamReader($JsonlFile)
    try {
        $linenum = 0
        while ($null -ne ($jsonline = $jsonstream.ReadLine())) {
            $linenum = $linenum + 1
            if($jsonline -match $regex){
                continue
            }

            try {
                $entry = $jsonline | ConvertFrom-Json
                if ([string]::IsNullOrWhiteSpace($entry.oupath)) {
                    $entry.oupath = $parentpath
                } else {
                    $entry.oupath = $entry.oupath + "," + $parentpath
                }

                if ([string]::IsNullOrWhiteSpace($entry.name)) {
                    Write-Error "${JsonlFile}:${linenum}: Error parametro name es requerido."
                }

                if ($entry.type -eq "group") {
                    if ([string]::IsNullOrWhiteSpace($entry.samaccount)) {
                        Write-Error "${JsonlFile}:${linenum}: Error parametro samaaccount es requerido para el type group."
                    }
                }

                $listou.Add($entry)
            } catch {
                Write-Error "${JsonlFile}:${linenum}: error de formato."
                throw $_
            } 
        }
    } finally {
        $jsonstream.Dispose()
    }

    if ($DryRun) {
        return
    }

    foreach ($entry in $listou)
    {
        $type = $entry.type
        $name = $entry.name
        $oupath = $entry.oupath
        $samaccount = $entry.samaccount

        $description =  $entry.description
        if ([string]::IsNullOrWhiteSpace($description)) {
            $description = ""
        }

        if ($type -eq "ou") {
            ## Validation, if the OU is already exist
            $ouidentity = "OU=" + $name + "," + $oupath
            Write-Output "Crear OU: $ouidentity"
            $oucheck = [adsi]::Exists("LDAP://$ouidentity")

            ## Condition of creation
            If($oucheck -eq "True"){
                Write-host -ForegroundColor Red "OU $name is already exist in the location $oupath"
            } Else {
                ## Create the OU with Accidental Deletion enabled
                Write-Output "Creating the OU $name ....."
                New-ADOrganizationalUnit -Name $name -Path $oupath
                Write-Host -ForegroundColor Green "OU $name is created in the location $oupath"
            }

            continue
        }

        if ($type -eq "group") {
            ## Validation, if the Group is already exist
            $ouidentity = "CN=" + $name + "," + $oupath
            Write-Output "Crear Grupo: $ouidentity"
            $oucheck = [adsi]::Exists("LDAP://$ouidentity")


            ## Condition of creation
            If($oucheck -eq "True"){
                Write-host -ForegroundColor Red "Group $name is already exist in the location $oupath"
            } Else {
                ## Create the OU with Accidental Deletion enabled
                Write-Output "Creating the Group $name ....."
                try {
                    New-ADGroup -Name "$name" -SamAccountName "$Realm-$samaccount-$name" -Path "$oupath" -GroupCategory Security -GroupScope Global -DisplayName "$name" -Description "$description" -ErrorAction Stop
                } catch [Microsoft.ActiveDirectory.Management.ADException] {
                    Write-host -ForegroundColor Red "Group $name is already exist in the location $oupath"
                }
                
                Write-Host -ForegroundColor Green "Group $name is created in the location $oupath"
            }

            continue
        }
    }
}
