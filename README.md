# sso-server-image

Image SSO server con tema de Contento.


## Temas

Los temas, base, keycloak, keycloak-preview estan como referencia a los que estan en la version de keycloack 11.
Solo estan como referencias para tener idea de como realizarlo.

## Proceso de CI.

Definimos el proceso de Integracion Continua, Creando la imagen del SSO Server y haciendola publica en el registry container de gitlab. De manera que pueda integrase facilemente con otros procesos y/o ambientes que requieran un SSO Server.

Asi mismo luego de construir la imagen, dependiendo la rama donde se haga el merge, se procede a disparar otro pipeline que corresponde al proceso de deploy de esa imagen.

## Creando nuevos artifact independientes

```
mvn -B archetype:generate -DgroupId=com.mycompany.app -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4
```
