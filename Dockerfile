ARG KEYCLOAK_VERSION=11.0.0
ARG JDK_IMAGE=openjdk:11.0.8-jdk

FROM ${JDK_IMAGE} as openjdk



FROM jboss/keycloak:${KEYCLOAK_VERSION} as keycloak

# Bug relacionado con SSL y keycload en App Engine.
# Intento de corregir SSL Problema con version original de keycloak en App engine
COPY --from=openjdk /usr/local/openjdk-11/conf/security/java.security /etc/alternatives/jre/conf/security/

LABEL MAINTAINER="Yohany Flores<yohanyflores@gmail.com>"

# Instalacion de themas
# Ojo si cambias la version de keycloak, debes evaluar los cambios a nivel de temas
ADD themes/contento /opt/jboss/keycloak/themes/contento


# Configuramos el favicon para los templates.
ADD tools/dockerfile/favicon-fix.sh /tmp/favicon-fix.sh
RUN /tmp/favicon-fix.sh

# Tema por defecto
ENV KEYCLOAK_DEFAULT_THEME contento
ENV KEYCLOAK_WELCOME_THEME contento

# Definimos variables de entorno hardcode.

ENV PROXY_ADDRESS_FORWARDING true
ENV JAVA_OPTS -server -Xms2048m -Xmx6144m -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=256m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true

EXPOSE 8080
