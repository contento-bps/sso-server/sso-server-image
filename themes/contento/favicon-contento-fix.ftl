<#-- BEGIN - FAVICON -->
<#if resourceCommonUrl?has_content>
    <#assign contentoFaviconBase = resourceCommonUrl?replace('/keycloak', '/contento')>
<#else>
    <#assign contentoFaviconBase = url.resourcesCommonPath?replace('/keycloak', '/contento')>
</#if>


<link rel="apple-touch-icon" sizes="57x57" href="${contentoFaviconBase}/img/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="${contentoFaviconBase}/img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="${contentoFaviconBase}/img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="${contentoFaviconBase}/img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="${contentoFaviconBase}/img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="${contentoFaviconBase}/img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="${contentoFaviconBase}/img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="${contentoFaviconBase}/img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="${contentoFaviconBase}/img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="${contentoFaviconBase}/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="${contentoFaviconBase}/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="${contentoFaviconBase}/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="${contentoFaviconBase}/img/favicon-16x16.png">
<link rel="manifest" href="${contentoFaviconBase}/img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="${contentoFaviconBase}/img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<#-- END - FAVICON -->
<#-- Importante terminar el archvio con un retorno de linea -->
